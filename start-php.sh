#!/usr/bin/env sh

PHP_VER=7
PHP_CONTAINER=caddy-demo
WORKING_DIR="$(realpath .)"

docker kill $PHP_CONTAINER
docker rm -f $PHP_CONTAINER

docker run \
  -d \
  --name=$PHP_CONTAINER \
  -v $WORKING_DIR:$WORKING_DIR \
  -v /tmp:/tmp \
  php:$PHP_VER-fpm \
    php-fpm \
      -y $WORKING_DIR/php-fpm.conf
