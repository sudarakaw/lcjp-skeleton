# Linux, Caddy, JSON, PHP (LCJP?) Skeleton Application

## Setup

```sh
COMPOSER=../composer.json composer install --no-dev -d www/

./start-php.sh

caddy run

```

## Usage

Open [http://127.0.0.1:5000/](http://127.0.0.1:5000/) on your web browser, or;

```sh
curl -i http://127.0.0.1:5000/api/search/leo

```

## License

Copyright 2020 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
