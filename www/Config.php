<?php
/**
 * src/Config.php: system configuration source file to build web/Config.php
 *
 * Copyright 2018 Fivestar IT (https://fivestarit.com/)
 *
 * Author: Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 */

class Config {
  // Operating environment
  const PRODUCTION_MODE   = false;

  // Time zone to be used in PHP environment
  const TIME_ZONE         = 'America/Los_Angeles';

  // Database access parameters
  const DB_HOST           = '%DB_HOST%';
  const DB_NAME           = '%DB_NAME%';
  const DB_USERNAME       = '%DB_USERNAME%';
  const DB_PASSWORD       = '%DB_PASSWORD%';

  // Secret keys
  const TOKEN_KEY         = '%TOKEN_KEY%';

  // VAPID keys for web-push
  const VAPID_PUBLIC_KEY  = '%VAPID_PUBLIC_KEY%';
  const VAPID_PRIVATE_KEY = '%VAPID_PRIVATE_KEY%';

  // Member parameters
  const ROLE_ID_GUEST     = 3;

  // Log settings
  const LOG_FILE          = '/tmp/bizref-main.log';
  const LOG_LEVEL         = \Lib\Log::OFF;
}
