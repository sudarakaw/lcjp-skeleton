(() => {

  'use strict'

  let _TIMER_ID = null

  const _INPUT_SERARCH_DELAY = 300 // in milliseconds

      , queueSearchRequest = ({ target }) => {
          if(_TIMER_ID) {
            clearTimeout(_TIMER_ID)
          }

          if(0 < target.value.length) {
            _TIMER_ID = setTimeout(preformSearch(target.value), _INPUT_SERARCH_DELAY)
          }
          else {
            updateMode()
          }
        }

      , preformSearch = query => () => {
          updateMode(query)

          fetch(`/api/search/${query}`)
            .then(ressponse => ressponse.json())
            .then(result => {
              if(result.error) {
                return Promise.reject(result.message)
              }

              return result.payload
            })
            .then(renderResult)
            .catch(showError)
        }

      , updateMode = mode => {
          let query = null

          const msg = document.querySelector('#resultArea p.alert-info')
              , error = document.querySelector('#resultArea p.alert-danger')
              , input = document.querySelector('#searchQuery')

          if(mode && 0 < mode.length) {
            query = mode
            mode = true
          }

          input.disabled = mode

          error.classList.add('d-none')

          msg.innerHTML = `Searching for <b>${query}</b>...`

          if(mode) {
            msg.classList.remove('d-none')
          }
          else {
            msg.classList.add('d-none')
          }
        }

      , showError = msg => {
          const error = document.querySelector('#resultArea p.alert-danger')
              , table = document.querySelector('#resultArea table')

          updateMode()

          table.classList.add('d-none')
          error.textContent = msg
          error.classList.remove('d-none')
        }

      , renderResult = records => {
          const table = document.querySelector('#resultArea table')
              , tbody = table.querySelector('tbody')

          tbody.innerHTML = records
            .map(({name, phone, email}) => (`
              <tr>
                <td>${name.first} ${name.last}</td>
                <td>${phone}</td>
                <td>${email}</td>
              </tr>
            `))
            .join('')

          updateMode()

          table.classList.remove('d-none')
        }

  document
    .querySelector('#searchQuery')
    .addEventListener('input', queueSearchRequest)

})()
