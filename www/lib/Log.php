<?php
/**
 * web/lib/Log.php: Record log entries to a disk file
 *
 * Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

namespace Lib;

final class Log {
  // Log message levels
  const INFO    = 10;
  const WARNING = 20;
  const ERROR   = 30;
  const OFF     = PHP_INT_MAX;

  // Cache (array of strings) that collect lines of text to write to the log
  // file.
  private $cache = null;

  // Additional headers to include in LogEntry
  private $additional_headers = [];


  private function __construct($additional_headers) {
    $filename = \Config::LOG_FILE;

    touch($filename);

    // Given file must exists with write access.
    if(!is_file($filename) || !is_writable($filename)) {
      throw new \Error('"' . $filename . '" is not a writable file.');
    }

    if(is_array($additional_headers)) {
      $this->additional_headers = $additional_headers;
    }

    $this->cache = [];
  }

  private function record($content, $level) {
    if(\Config::LOG_LEVEL > $level) {
      return;
    }

    $this->cache[] = LogEntry::create($content, $level, $this->additional_headers);
  }

  public function __destruct() {
    if(empty($this->cache)) {
      // Nothing to write.

      return;
    }

    $file_resource = @fopen(\Config::LOG_FILE, 'a+');
    if(is_resource($file_resource)) {
      fwrite
        ( $file_resource
        , join('', $this->cache)
        );

      fclose($file_resource);
    }
  }

  public function info($content) {
    $this->record($content, Log::INFO);
  }

  public function warning($content) {
    $this->record($content, Log::WARNING);
  }

  public function error($content) {
    $this->record($content, Log::ERROR);
  }

  public static function create($additional_headers = []) {
    return new Log($additional_headers);
  }
}


final class LogEntry {
  // Log entry body
  private $content = null;

  // Log level for this entry
  private $level = null;

  // Log entry headers
  private $headers = null;

  private function __construct($content, $level, $additional_headers) {
    $this->sanitize($content);

    $this->level = $level;

    $this->headers =
      [ 'date/time' => date('Y-m-d H:i:s (e, T, \U\T\C P)', time())
      , 'user agent' => $_SERVER['HTTP_USER_AGENT']
      , 'ip' => $_SERVER['REMOTE_ADDR']
      ];


    if(is_array($additional_headers)) {
      $this->headers = array_merge($this->headers, $additional_headers);
    }
  }

  private function sanitize($content) {
    if(is_null($content)) {
      $content = PHP_EOL;
    }
    elseif(is_object($content) && method_exists($content, '__toString')) {
      $content .= '';
    }
    elseif(is_object($content) || is_array($content)) {
      $content = json_encode($content, JSON_PRETTY_PRINT);
    }

    $this->content = $content;
  }


  public function __set($key, $value) {
    // No-op
  }

  public function __get($key) {
    if('level' === $key) {
      return $this->level;
    }
    else {
      return null;
    }
  }

  public function __toString() {
    $headers = [];

    $footer =
      [ str_repeat('=', 80)
      , null
      ];

    // Get the length of longest header key
    $key_max = array_reduce
      ( array_keys($this->headers)
      , function($max, $key) { return max($max, strlen($key)); }
      , 0
      );

    foreach($this->headers as $key => $value) {
      $headers[] = strtoupper(str_pad($key, $key_max, ' ')) . ' : ' . $value;
    }

    $headers[] = str_repeat('-', 80);

    return join(PHP_EOL, array_merge($headers, [ $this->content ], $footer));
  }


  public static function create($content, $level = Log::INFO, $additional_headers = []) {
    return new LogEntry($content, $level, $additional_headers);
  }
}
