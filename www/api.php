<?php
/**
 * web/index.php: main entry file for API
 *
 * Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

require_once('vendor/autoload.php');

\Api\Core::run();
