<?php
/**
 * web/api/Auth.php: request authorization subroutines for API core
 *
 * Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

namespace Api;

use DB\Connection;
use Entity\Member;
use Lib\JWT;

trait Auth {
  protected $authorizedMember = false;

  protected final function require_capabilities($capabilities, $message = 'access denied') {
    if(!$this->is_capable($capabilities)) {
      self::sendMessage
        ( 403
        , $message
        );
    }
  }

  protected final function is_capable($capabilities) {
    if(false === $this->authorizedMember) {
      return false;
    }

    return $this->authorizedMember->is_capable($capabilities);
  }

  private function authorize() {
    $token = null;

    if(isset($_SERVER['HTTP_AUTHORIZATION'])) {
      $token = preg_replace('/^bearer\s+/i', '', $_SERVER['HTTP_AUTHORIZATION']);
    }
    elseif(isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
      $token = preg_replace('/^bearer\s+/i', '', $_SERVER['REDIRECT_HTTP_AUTHORIZATION']);
    }

    if(!empty($token)) {
      $payload = JWT::payload($this->secrets['TOKEN'], $token);

      if(isset($payload->id) && is_numeric($payload->id)) {
        $this->authorizedMember = Member::from(Connection::create())
          ->with_fields([ 'roles', 'capabilities' ])
          ->getOne([ ':id' => $payload->id ]);
      }
    }
  }

  private function forbidUnauthorizedAccess() {
    if(false === $this->authorizedMember) {
      self::sendMessage
        ( 403
        , 'invalid access token'
        );
    }
  }
}
