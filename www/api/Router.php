<?php
/**
 * web/api/Router.php: handle initial processing of the HTTP request
 *
 * Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

namespace Api;

trait Router {
  private static $HANDLER_PATH = './handlers/';

  private static $validVerbs = [ 'get', 'post', 'put', 'patch', 'delete', 'options' ];

  private static function getHandlerFile($httpVerb, $uri) {
    $params = [];
    $uriBlock = null;
    $checkDir = self::$HANDLER_PATH;

    if(!in_array($httpVerb, self::$validVerbs, true)) {
      self::sendMessage(
        501,
        'HTTP ' . strtoupper($httpVerb) . ' method is not supported'
      );
    }

    // Reverse URI block list so we can process it by pop items from end of the
    // list (pop vs shift performance)
    $uriQueue = array_reverse($uri);

    while(!empty($uriQueue)) {
      $uriBlock = array_pop($uriQueue);

      if(!empty($uriBlock)) {
        if(is_dir($checkDir . $uriBlock)) {
          $checkDir .= $uriBlock . DIRECTORY_SEPARATOR;
        }
        else {
          // URI block(s) that does not correspond to a directory in file system
          // are considered parameters in the URI.
          $params[] = $uriBlock;
        }
      }
    }

    if(0 !== strncasecmp(self::$HANDLER_PATH, $checkDir, max([
      strlen(self::$HANDLER_PATH),
      strlen($checkDir)
    ]))) {
      // api handler: ./dir/verb.php
      $checkFile = $checkDir . $httpVerb . '.php';

      if(is_file($checkFile)) {
        return [
          'file' => $checkFile,
          'helper' => self::findHelper($checkDir),
          'params' => $params
        ];
      }
      elseif('options' == $httpVerb) {
        $allowedVerbs = [];
        $dirHandle = opendir($checkDir);

        while(false !== ($file = readdir($dirHandle))) {
          if(!is_file($checkDir . DIRECTORY_SEPARATOR . $file)) {
            continue;
          }

          $info = pathinfo($file);

          if('php' != $info['extension']) {
            continue;
          }

          if(!in_array($info['filename'], self::$validVerbs, true)) {
            continue;
          }

          $allowedVerbs[] = strtoupper($info['filename']);
        }

        return $allowedVerbs;
      }
    }

    return null;
  }

  private static function getUrl() {
    $https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '';
    $protocol = 'http' . (empty($https) || 'off' === $https ? '' : 's');
    $host = empty($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['SERVER_NAME'];
    $port = in_array($_SERVER['SERVER_PORT'], array(80, 443)) ? null : $_SERVER['SERVER_PORT'];
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

    return strtolower($protocol . '://' . $host . (empty($port) ? '' : ':' . $port) . $path);
  }

  public static final function run() {
    @session_unset();
    @session_abort();

    if(defined('\Config::PRODUCTION_MODE') && true === \Config::PRODUCTION_MODE) {
      error_reporting(E_ERROR | E_CORE_ERROR);
    }
    else {
      error_reporting(E_ALL);
    }

    $verb = strtolower($_SERVER['REQUEST_METHOD']);

    // Use URI excluding the query string
    $fullUrl = self::getUrl();
    $uri = parse_url($fullUrl, PHP_URL_PATH);
    $uri = explode('/', $uri);

    // Exclude first two parts of the URI blocks which is always [ "", "api" ]
    // because of the leading slash and keyword "/api/"
    $uri = array_slice($uri, 2);

    $handler = self::getHandlerFile($verb, $uri);

    // if we can't find a handler file matching the current URI:
    if(!is_array($handler)) {
      self::sendMessage(
        501,
        'API handler for HTTP ' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'] . ' does not exists'
      );
    }

    if('options' == $verb) {
      self::sendMessage(
        200,
        '',
        [
          [ 'Allow', join(',', $handler) ] ,
          [ 'Access-Control-Allow-Methods', join(',', $handler) ]
        ],
        true // without body
      );
    }

    if(!empty($handler['helper'])) {
      @include_once($handler['helper']);
    }

    @include_once($handler['file']);

    // If the handler file found does not contain an \Handler\Api class:
    if(!class_exists('\Handler\Api', false)) {
      self::sendMessage(
        500,
        'API handler for HTTP ' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'] . ' is invalid'
      );
    }

    $apiHandler = new \Handler\Api($verb, $handler['params'], $fullUrl);

    // If the class \Handler\Api does not extend \Api\Core:
    if(!($apiHandler instanceof \Api\Core)) {
      self::sendMessage(
        500,
        'API handler for HTTP ' . $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'] . ' is not properly implemented'
      );
    }

    $apiHandler->respond();
  }

  public static final function findHelper($dir) {
    if(!is_dir($dir) || strlen(self::$HANDLER_PATH) > strlen($dir)) {
      return null;
    }

    $checkFile = $dir . 'helper.php';

    if(is_file($checkFile)) {
      return $checkFile;
    }

    return self::findHelper(dirname($dir));
  }
}
