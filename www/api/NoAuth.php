<?php
/**
 * web/api/NoAuth.php: disable request authorization subroutines of Auth trait
 *
 * Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

namespace Api;

trait NoAuth {}
