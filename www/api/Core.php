<?php
/**
 * web/api/Core.php: core implementation of API system
 *
 * Copyright 2018, 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

namespace Api;

use Lib\Log;

class Core {
  use Router;
  use Auth;

  private const HTTP_STATUS_MAP = [
    200 => 'OK',
    201 => 'Created',

    400 => 'Bad Request',
    401 => 'Unauthorized',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    409 => 'Conflict',

    500 => 'Internal Server Error',
    501 => 'Not Implemented'
  ];

  private $request =
    [ 'url'     => null   // Full URL of the API call being processed
    , 'user'    => null   // Authorized POS Admin user object
    , 'db'      => null   // Database access object
    , 'params'  => null   // Remainder of the URI after handler resolution
    , 'query'   => null   // HTTP GET parameters
    , 'data'    => null   // HTTP POST or JSON content
    , 'log'     => null   // Instance of Lib\Log to record activity to disk file.
    ];

  private $output = [
    'error' => false,
    'status' => 200,
    'message' => '',
    'headers' => [],
    'payload' => null,
    'emptyBody' => false
  ];


  private function __construct($verb, $parameters, $url = '') {
    $this->request['url'] = $url;
    $this->request['params'] = $parameters;
    $this->request['secrets'] = [
      'TOKEN' => \Config::TOKEN_KEY
    ];

    putenv(\Config::TIME_ZONE);
    date_default_timezone_set(\Config::TIME_ZONE);

    if('Api\Core' !== get_called_class()) {
      $this->authorize();

      if(!in_array('Api\NoAuth', class_uses(get_called_class()))) {
        $this->forbidUnauthorizedAccess();
      }
    }

    $this->request['log'] = Log::create
      ( [ 'request' => strtoupper($verb) . ' ' . $this->url
        , 'member' =>
            ( is_object($this->authorizedMember)
            ? trim($this->authorizedMember->member_name . ' (' . $this->authorizedMember->member_id . '/' . $this->authorizedMember->id . ')')
            : 'n/a'
            )
        ]
      );

    // request[query] should have a clone of $_GET
    if(isset($_GET)) {
      $this->request['query'] = array_combine
        ( array_keys($_GET)
        , $_GET
        );
    }

    $rawData = file_get_contents('php://input');

    if(0 === strncmp('delete',  $verb, max(6, strlen($verb))) && !empty($rawData)) {
      parse_str($rawData, $this->request['data']);
    }

    if(in_array('application/json', [
      @$_SERVER['HTTP_CONTENT_TYPE'],
      @$_SERVER['CONTENT_TYPE']
    ])) {
      // request[data] should have decode JSON
      $this->request['data'] = json_decode($rawData, true);
    }
    else {
      // request[data] should have a clone of $_POST & $_FILES
      $post = [];
      $files = [];

      if(isset($_POST)) {
        $post = array_combine
          ( array_keys($_POST)
          , $_POST
          );
      }

      if(isset($_FILES)) {
        $files = array_combine
          ( array_keys($_FILES)
          , $_FILES
          );
      }

      $this->request['data'] = array_merge($post, $files);
    }

    // Global $_GET, $_POST, $_FILES and $_REQUEST will no longer be available
    unset($_GET);
    unset($_POST);
    unset($_FILES);
    unset($_REQUEST);

    $this->init();

    $this->payload = $this->handle();
  }

  public final function __destruct() {
    $this->cleanup();

    if(is_object($this->db)) {
      unset($this->db);
    }
  }


  public final function __set($name, $value) {
    // output members
    if(array_key_exists($name, $this->output)) {
      $this->output[$name] = $value;
    }
  }

  public final function __get($name) {
    // request members
    if(array_key_exists($name, $this->request)) {
      return $this->request[$name];
    }
  }


  protected final function appendHeaders($headers) {
    if(!is_array($headers)) {
      return;
    }

    $this->output['headers'] = array_merge($this->output['headers'], $headers);
  }


  private static final function sendMessage($status, $message, $headers = [], $withoutBody = false) {
    $api = new Core(null, null);

    $api->status = $status;
    $api->message = $message;
    $api->appendHeaders($headers);

    $api->respond($withoutBody);
  }

  private function respond($withoutBody = false) {
    header($_SERVER['SERVER_PROTOCOL'] . ' ' . $this->output['status'] . ' ' . self::HTTP_STATUS_MAP[$this->output['status']]);
    header('Content-type: application/json');

    foreach($this->output['headers'] as $header) {
      if(is_array($header)) {
        $header = join(': ', array_slice($header, 0, 2));
      }

      header($header);
    }

    $this->output['error'] = 400 <= $this->output['status'];

    if(!$withoutBody && !$this->output['emptyBody']) {
      unset($this->output['status']);
      unset($this->output['headers']);
      unset($this->output['emptyBody']);

      echo json_encode($this->output);
    }

    exit;
  }


  // WARNING: DO NOT IMPLEMENT ANY FUNCTIONALITY IN FOLOWING METHODS
  // There is NO GUARANTEE they will called by the API handlers if extended.
  protected function handle() {}
  protected function init() {}
  protected function cleanup() {}
}
