<?php

namespace Handler;


class Api extends \Api\Core {
  use \Api\NoAuth;

  protected function handle() {
    if(!isset($this->params[0]) || empty($this->params[0]))  {
      $this->status = 400;
      $this->message = 'Empty query received.';

      return;
    }

    $query = trim($this->params[0]);

    $records = json_decode(file_get_contents('../data.json'));

    $records = array_filter
      ( $records
      , function($rec) use($query) {
          return false !== stripos($rec->name->first, $query) || false !== stripos($rec->name->last, $query);
        }
      );

    if(empty($records))  {
      $this->status = 404;
      $this->message = 'No matching records';

      return;
    }

    return array_values($records);
  }
}
